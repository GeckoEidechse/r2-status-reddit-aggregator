# R2 Status Reddit Aggregator

Collect status information about Titanfall 2 servers using reddit user feedback and display results on webpage

## Setup

- Setup Python virtual environment:

```bash
python3 -m venv titanfall-status
```

- Activate Python virtual environment

```bash
source titanfall-status/bin/activate
```

- Install pip dependencies:

```bash
pip install -r requirements.txt
```

- Fill out `src/praw.ini.example` and rename to `src/praw.ini`

- Go to `src/` directory

```bash
cd src/
```

- Run Python application

```bash
python main.py
```
