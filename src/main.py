import praw
import json
import threading
import time
from pathlib import Path
import yaml
import os
from flask import Flask, redirect, url_for, render_template
import urllib
DEFAULT_CONFIG_FILE = "config.yaml"


app = Flask(__name__)


@app.route('/.well-known/security.txt')
def security_txt():
    return "Please create a *confidential* issue in the GitLab repo: https://gitlab.com/GeckoEidechse/r2-status-reddit-aggregator/-/issues/new"


def encode(text: str) -> str:
    return urllib.parse.quote(text, safe="")


@app.route('/')
def web_main():
    with open("status.json", "rt") as f:

        result = json.loads(str(f.read()))

    disclaimer = "\nDisclaimer: By sending your message you agree to me permanently storing it on my server together with its metadata (timestamp, sender, ...). If you do not want this, don't send a message to this bot!\n"

    reddit_dm_dict = dict()
    for platform in config_data['platform']:
        reddit_dm_dict[platform] = dict()
        for status in config_data['status']:
            content = f'platform: {platform}\nstatus: {status}\n'
            total_string = f"https://np.reddit.com/message/compose/?to={encode('titanfall-status-bot')}&subject={encode(config_data['subject'])}&message={encode(content)}{encode(disclaimer)}"
            reddit_dm_dict[platform][status] = total_string

    return render_template("main.html", result=result, reddit_dm_dict=reddit_dm_dict)


@app.errorhandler(404)
def handle_404(e):
    return redirect(url_for("web_main"))


def webserver():
    # Present webpage
    app.run()


def store_json_response(json_string: str, timestamp: int, id: str):
    global config_data

    # Make filename
    filename = f"{timestamp}-{id}.json"

    # Create folder if not exists
    Path(config_data["data-store-location"]).mkdir(parents=True, exist_ok=True)

    # Write to file
    with open(config_data["data-store-location"] + filename, "wt") as f:
        f.write(json_string)


def in_ignore_list(current_json):
    # TODO add ignore factors here
    return False


def parse_data():
    global config_data

    # Run over messages...
    # ...parse...
    # ...update website
    # Wait

    # On very first run we won't have the data-store-location folder yet. Make it just in case
    Path(config_data["data-store-location"]).mkdir(parents=True, exist_ok=True)

    dir_list = sorted(os.listdir(config_data["data-store-location"]))

    status_dict = dict()
    for platform in config_data["platform"]:
        status_dict[platform] = dict()
        for status in config_data["status"]:
            status_dict[platform][status] = 0

    files = [config_data["data-store-location"] + elem for elem in dir_list]
    for file in dir_list:
        timestamp = int(file[:-13])  # Strip `-<id>.json`

        # Stop early if too old
        if timestamp < int(time.time()) - 60*60*24*2:
            continue

        with open(config_data["data-store-location"] + file, "rt") as f:
            try:
                current_json = json.load(f)
            except json.JSONDecodeError:
                print("Failed parsing JSON ")
                continue

        if current_json["subject"] != config_data["subject"]:
            continue

        if in_ignore_list(current_json):
            continue

        for platform in config_data["platform"]:
            if f"platform: {platform}".lower() in current_json["body"].lower():
                for status in config_data["status"]:
                    if f"status: {status}".lower() in current_json["body"].lower():
                        status_dict[platform][status] += 1
    with open("status.json", "wt") as f:
        f.write(json.dumps(status_dict))


def bot():
    # Init bot
    reddit = praw.Reddit("titanfall-status-bot")
    # Loop
    while True:
        print("Checking reddit")
        # Get unread messages
        messages = reddit.inbox.unread()

        got_messages = False
        for message in messages:
            got_messages = True
            # Store them
            response_as_json = json.dumps(
                {key: (str(val) if key in ["_reddit", "dest", "author"] else val) for key, val in message.__dict__.items()}
            )
            store_json_response(response_as_json, int(message.created_utc), message.id)
            message.mark_read()

        # Parse received messages
        parse_data()

        print("Sleeping")
        time.sleep(60)


def main():
    global config_data

    # Read config file
    with open(DEFAULT_CONFIG_FILE, "r") as stream:
        config_data = yaml.safe_load(stream)

    # Thread 1: Webserver
    thread_webserver = threading.Thread(target=webserver, args=())
    thread_webserver.start()

    # Thread 2: Reddit bot
    thread_bot = threading.Thread(target=bot, args=())
    thread_bot.start()

    # Wait until finished
    thread_webserver.join()
    thread_bot.join()

def init_bot() -> praw.Reddit:
    return praw.Reddit("titanfall-status-bot")


if __name__ == "__main__":
    main()
